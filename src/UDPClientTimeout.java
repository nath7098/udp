public class UDPClientTimeout extends UDPClientBuilder implements Runnable{
    UDPClientTimeout() {
    }
    public void run() {
        try {
            this.setConnection(); //Initialise la connexion socket
            this.rep = this.getReceivingPacket(2048); //Attends une connexion entrante
            this.s.receive(this.rep); //Reception de la connexion
            this.s.close(); //Ferme la connexion client
        } catch (Exception var2) {
            System.out.println("Exception TooManyTime UDPClient");
        }

    }
}
