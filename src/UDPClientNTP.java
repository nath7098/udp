public class UDPClientNTP extends UDPClientBuilder implements Runnable {
    private long ts0;
    private long ts1;

    @Override
    public void run() {
        try {
            setConnection();
            System.out.println("UDPClientNTP launched ....");
            while (true) {
                req = getSendingPacket(isA, 2048);
                ts0 = getLocalTime();
                s.send(req);
                rep = getReceivingPacket(2048);
                s.receive(rep);
                ts1 = getLocalTime();
                System.out.println(toms(ts1 - ts0) + "\t ms");
                Thread.sleep(250L);
            }
        } catch (Exception var2) {
            this.close();
        }
    }

    private void close() {
        this.s.close();
        System.out.println("Exception UDPClient");
    }

    private double toms(long var1) {

        return (double) var1 / 1000000.0D;
    }
}
