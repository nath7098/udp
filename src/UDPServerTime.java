import java.net.InetSocketAddress;

public class UDPServerTime extends UDPServerBuilder implements Runnable {
    public void run(){
        try
        {
            setConnection();
            System.out.println("UDPServerTime launched ....");

            do {
                req = getReceivingPacket(2048);
                s.receive(req);
                System.out.println("Timestamp delivered ...");
                rep = getSendingPacket((InetSocketAddress)req.getSocketAddress(), 2048);
                setTimeStamp(rep);
                s.send(rep);
            }while(true);
        } catch (Exception var2) {
            s.close();
            System.out.println("Exception UDPServer");
        }
    }
}
