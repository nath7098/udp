import java.io.IOException;

public class UDPClientInfo extends UDPClientBuilder implements Runnable {
    public void run() {
        try {
            setConnection();
            socketInfo("client sets the connection",s);
            s.close();
            socketInfo("client closes the connection",s);
        }
        catch(IOException e)
        { System.out.println("IOException UDPClient"); }
    }
}
/*while(true) {
        System.out.println("UDPServerHello waiting ....");
        this.socketInfo("hello server", this.s);
        this.req = new DatagramPacket(new byte[2048], 2048);
        this.s.receive(this.req);
        System.out.println("request received");
        this.rep = new DatagramPacket(new byte[2048], 0, 2048, this.req.getSocketAddress());
        this.s.send(this.rep);
        System.out.println("reply sent");
        }
        } catch (Exception var2) {
        if (this.s != null) {
        this.s.close();
        }*/
