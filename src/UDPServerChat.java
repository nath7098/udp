import java.net.InetSocketAddress;

public class UDPServerChat extends UDPServerBuilder implements Runnable{
    private String msg;
    //ECHANGE DE MESSAGE EN DUR


    public void run() {
        try {
            setConnection();
            do {
                req = getReceivingPacket(100);//Le serveur attends un message avec une taille fixe
                s.receive(req); //Valide la reception par le serveur
                msg = getMsg(req);
                System.out.println("Message reçu provenant du Client: " + msg);
                rep = getTextSendingPacket((InetSocketAddress) req.getSocketAddress(), "Reçu", 100); ///Meme chose que SendingPacket mais envoie un texte avec la requete
                s.send(rep);
            }while (true);
        } catch (Exception var2) {
            s.close();
            System.out.println("\nException UDPServer");
        }
    }
}
