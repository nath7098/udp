public class UDPServerTimeout extends UDPServerBuilder implements Runnable{
    UDPServerTimeout() {
    }
    public void run() {
        try {
            this.setConnection();
            this.rep = this.getReceivingPacket(2048);
            this.s.receive(this.rep);
            this.s.close();
        } catch (Exception var2) {
            System.out.println("Exception TooManyTime UDPServer");
        }

    }
}
