import java.net.InetSocketAddress;

public class UDPServerNTP extends UDPServerBuilder implements Runnable {
    private long ts0;
    private long ts1;
    @Override
    public void run() {
        try {
            setConnection();
            System.out.println("UDPServerNTP launched ....");

            while(true) {
                req = getReceivingPacket(2048);
                s.receive(req);
                rep = getSendingPacket((InetSocketAddress)req.getSocketAddress(), 2048);
                s.send(rep);
            }
        } catch (Exception var2) {
            this.s.close();
            System.out.println("Exception UDPServer");
        }
    }
}
