import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;

class UDPServerHello extends UDPServerBuilder implements Runnable {


    /** The main run method for threading. */
    public void run( ) {
        try {
            s = new DatagramSocket(isA.getPort());
            req = new DatagramPacket(new byte[size],size);
            s.receive(req);
            System.out.println("request received");
            rep = new DatagramPacket(new byte[size],0,size,req.getSocketAddress());
            s.send(rep);
            System.out.println("reply sent");
            s.close();
        }
        catch(IOException e)
        { System.out.println("IOException UDPServer"); }
    }

}