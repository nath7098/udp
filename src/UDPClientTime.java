public class UDPClientTime extends UDPClientBuilder implements Runnable {

    private long ts0;
    private long ts1;
    private long ts2;
    @Override
    public void run() {
        try {
            setConnection();
            System.out.println("UDPClientTime launched ....");
            ts0 = getTimeStamp();

            while(true) {
                req = getSendingPacket(isA, 2048);
                s.send(req);
                ts1 = getTimeStamp(); //Envoie un paquet au serveur puis evalue le temps entre l'envoie et le retour du serveur
                rep = getReceivingPacket(2048);
                s.receive(rep);
                ts2 = getTimeStamp(rep);
                System.out.println(tos(ts1 - ts0) + "\t s \t" + (ts2 + 1L - ts1) + "\t ms");
                Thread.sleep(250L);
            }
        } catch (Exception var2) {
            close();
        }
    }
    private void close() {
        this.s.close();
        System.out.println("Exception UDPClient");
    }

    private double tos(long var1) {
        return (double)var1 / 1000.0D;
    }
}
