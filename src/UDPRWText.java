import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetSocketAddress;

public class UDPRWText extends UDPInfo{
    private byte[] sB;
    /** The buffer array. */
    /** To prepare a sending packet at a given size. */
    protected DatagramPacket getSendingPacket(InetSocketAddress isAR, int size) throws IOException
    {
        return new DatagramPacket(new byte[size],0,size,isAR.getAddress(),isAR.getPort()); //Envoie dans la methode getSendingPacket
        //la taille du buffer du message, le message avec l'adresse et le port de destination
    }
    /** To prepare a receiving packet at a given size. */
    protected DatagramPacket getReceivingPacket(int size) throws IOException
    {
        return new DatagramPacket(new byte[size],size); //Recois le message avec la taille du buffer
    }
    /** To create a sending packet send with a txt message. */
    protected DatagramPacket getTextSendingPacket(InetSocketAddress isA, String msg, int size) throws
            IOException {
        sB = toBytes(msg, new byte[size]);
        return new DatagramPacket(sB,0,sB.length,isA.getAddress(),isA.getPort());
    }
    /** To set the Msg to a parametter packet. */
    protected void setMsg(DatagramPacket dP, String msg) throws IOException
    { toBytes(msg, dP.getData()); }
    private byte[] toBytes(String msg, byte[] lbuf) {
        array = msg.getBytes();
        if(array.length < lbuf.length)
            for(int i=0;i<array.length;i++)
                lbuf[i] = array[i];
        return lbuf;
    }
    private byte[] array;
    /** To extract the txt message from a packet. */
    protected String getMsg(DatagramPacket dP) {
        sB = dP.getData();
        for(int i=0;i<sB.length;i++) {
            if(sB[i] == 0)
            { p = i; i = sB.length; }
        }
        return new String(dP.getData(),0,p);
    }
    private int p;
}
