import java.net.DatagramPacket;
import java.net.InetSocketAddress;
import java.util.Scanner;

public class UDPClientChat extends UDPClientBuilder implements Runnable {
    private Scanner sc;
    private String msg = "Bonjour";
    private String recept;
    //ECHANGE MESSAGE EN DUR
    //Pas besoin de creer d'objet car heritage de UDPClientBuilder

    public void run() {
        try {
            setConnection();
            do {
                sc = new Scanner(System.in);
                System.out.println(">>");
                msg = sc.nextLine();
                req = getTextSendingPacket(isA, msg, 100); //La taille est celle maximum d'un envoie
                s.send(req); //On envoie un msg au serveur
                rep = getReceivingPacket(100);
                s.receive(rep);
                recept = getMsg(rep);
                System.out.println("Message reçu provenant du Serveur : " + recept);
            } while (true);
        } catch (Exception var2) {
            sc.close();
            s.close();
            System.out.println("\nException UDPClient");
        }
    }
}
