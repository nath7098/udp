import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.*;
class UDPClientBuilder extends UDPRWTime{
    protected InetSocketAddress isA;
    protected DatagramSocket s;
    protected DatagramPacket req, rep;
    protected final int size = 2048;
    private int timer = 20000;

// the remote address
// the socket object
// to prepare the request and reply messages
// the default size for the buffer array
    /** The builder. */
    UDPClientBuilder() {
        isA = new InetSocketAddress("192.168.43.92",8080); //Adresse distante du serveur avec son port
        s = null; req = rep = null;
    }
    protected void setConnection() throws IOException {
        s = new DatagramSocket();
        isA = new InetSocketAddress("192.168.43.92",8080);
        s.setSoTimeout(timer); //Permet de mettre un timer de temps de reponse attendu sinon requete client echoue
/** we can include more setting, later ... */
    }

    public static void main(String[] args) //Les methodes de thread doivent etre lancé dans le thread main
    {
        new Thread(new UDPClientNTP()).start();
        new Thread(new UDPServerNTP()).start();
    }
}