import org.omg.PortableServer.THREAD_POLICY_ID;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;

public class UDPServerBuilder extends UDPRWTime{
    protected InetSocketAddress isA; //Adresse du client
    protected DatagramSocket s;
    protected DatagramPacket req, rep;
    protected final int size = 2048;
    private int timer = 2000;

    /** The builder. */
    UDPServerBuilder() {
        isA = new InetSocketAddress("localhost",8080);
        s = null; req = rep = null;
    }
// the address
// the socket object
// to prepare the request and reply messages
// the default size for the buffer array
//Uniquement en mode local pour le serveur et  client
    protected void setConnection() throws IOException {
        s = new DatagramSocket(8080);
        isA = new InetSocketAddress("localhost",8080);
        s.setSoTimeout(50000); //Le client se ferme avant le serveur si aucune connexion
/** we can include more setting, later ... */
    }
    public static void main(String[] args)
    {
        /*new Thread(new UDPServerHello()).start();
        new Thread(new UDPClientHello()).start();*/
        new Thread(new UDPServerTime()).start();

       // new Thread(new UDPClientTime()).start();

    }
}
