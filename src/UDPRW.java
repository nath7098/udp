//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetSocketAddress;

class UDPRW extends UDPInfo {
    private byte[] sB;
/** The buffer array. */
/** To prepare a sending packet at a given size. */
protected DatagramPacket getSendingPacket(InetSocketAddress isAR, int size) throws IOException
    {
        return new DatagramPacket(new byte[size],0,size,isAR.getAddress(),isAR.getPort()); //Envoie dans la methode getSendingPacket
        //la taille du buffer du message, le message avec l'adresse et le port de destination
    }
    /** To prepare a receiving packet at a given size. */
    protected DatagramPacket getReceivingPacket(int size) throws IOException
    {
        return new DatagramPacket(new byte[size],size); //Recois le message avec la taille du buffer
    }

}

